FROM node:lts as runtime

ADD /external/gatsby-starter-blog /touchdown
COPY bin/touchdown /bin/touchdown
RUN chmod +x /bin/touchdown

WORKDIR /touchdown

RUN npm install
RUN npm run build

RUN ls -al .
RUN rm -rf public

RUN node -v && npm  -v

# ENTRYPOINT ["./entrypoint.sh"]
